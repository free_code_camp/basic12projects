puzzle = [
    [-1, 3, -1, -1, -1, -1, -1, 5, -1],
    [-1, -1, 8, -1, 9, 1, 3, -1, -1],
    [6, -1, -1, 4, -1, -1, 7, -1, -1],
    [-1, -1, 3, 8, 1, -1, -1, -1, -1],
    [-1, -1, 6, -1, -1, -1, 2, -1, -1],
    [-1, -1, -1, -1, 3, 4, 8, -1, -1],
    [-1, -1, 1, -1, -1, 8, -1, -1, 9],
    [-1, -1, 4, 1, 2, -1, 6, -1, -1],
    [-1, 6, -1, -1, -1, -1, -1, 4, -1]
]

solution = [
    [1, 3, 9, 7, 6, 2, 4, 5, 8],
    [7, 4, 8, 5, 9, 1, 3, 2, 6],
    [6, 5, 2, 4, 8, 3, 7, 9, 1],
    [5, 2, 3, 8, 1, 6, 9, 7, 4],
    [4, 8, 6, 9, 5, 7, 2, 1, 3],
    [9, 1, 7, 2, 3, 4, 8, 6, 5],
    [2, 7, 1, 6, 4, 8, 5, 3, 9],
    [3, 9, 4, 1, 2, 5, 6, 8, 7],
    [8, 6, 5, 3, 7, 9, 1, 4, 2]
]

def find_next_empty(puzzle):
    for r in range(9):
        for c in range(9):
            if puzzle[r][c] == -1:
                return r,c 
    return None, None

def is_valid(puzzle, guess, row, col):
    row_values = puzzle[row]
    if guess in row_values:
        return False
    
    col_values = [puzzle[i][col] for i in range(9)]
    if guess in col_values:
        return False
    
    row_start_idx = (row // 3) * 3
    col_start_idx = (col // 3) * 3
    for r in range(row_start_idx, row_start_idx + 3):
        for c in range(col_start_idx, col_start_idx + 3):
            if puzzle[r][c] == guess:
                return False

    return True

def solve_sudoku(puzzle):
    row, column = find_next_empty(puzzle)
    
    if row is None:
        return True

    for guess in range(1,10):
        if is_valid(puzzle, guess, row, column):
            puzzle[row][column] = guess

            if solve_sudoku(puzzle):
                return True

        puzzle[row][column] = -1

    return False

if __name__ == '__main__':
    print(solve_sudoku(puzzle))
    print(puzzle)
