def native_search(l, target):
    for i in range(len(l)):
        if l[i] == target:
            return i
    return -1


def binary_search(l, t, low=None, high=None):

    if low is None:
        low = 0
    if high is None:
        high = len(l)-1
    if high < low:
        return -1
    print("🐍  ~ low", low)
    print("🐍  ~ high", high)

    mid = (low + high) // 2
    print("🐍 File: basic12projects/binary_search.py | Line: 20 | binary_search ~ mid", mid)
    print("🐍~ l[mid", l[mid])

    if l[mid] == t:
        return mid
    elif t < l[mid]:
        return binary_search(l, t, low, mid-1)
    else:
        return binary_search(l, t, mid+1, high)


if __name__ == '__main__':
    l = [1, 2, 3, 4, 6, 32, 46, 92, 123, 132]
    t = 123
    print(native_search(l, t))
    print(binary_search(l, t))
