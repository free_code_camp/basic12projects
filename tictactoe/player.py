import math
import random


class Player:
    def __init__(self, letter):
        self.letter = letter

    def get_move(self, game):
        pass


class RandomComputerPlayer(Player):
    def __init__(self, letter):
        super().__init__(letter)

    def get_move(self, game):
        square = random.choice(game.available_moves())
        return square


class HumanPlayer(Player):
    def __init__(self, letter):
        super().__init__(letter)

    def get_move(self, game):
        valid_square = False
        value = None
        while not valid_square:
            square = input(self.letter + '\'s turn. Input number (0-9): ')
            try:
                value = int(square)
                if value not in game.available_moves():
                    raise ValueError
                valid_square = True
            except ValueError:
                print('Invalid square...')

        return value


class GeniusComputerPlayer(Player):
    def __init__(self, letter):
        super().__init__(letter)

    def get_move(self, game):
        if game.available_moves() == 9:
            square = random.choice(game.available_moves())
        else:
            square = self.minimax(game, self.letter)['position']  # !!!
        return square

    def minimax(self, state, player):
        max_player = self.letter
        oponent = 'O' if player == 'X' else 'X'

        if state.current_winner == oponent:
            return {'position': None,
                    'score': 1 * (state.num_empty_squares()+1) if oponent == max_player else 1 * (state.num_empty_squares()-1)}

        elif not state.has_empty_squares():
            return {'position': None,
                    'score': 0}

        if player == max_player:
            best = {'position': None,
                    'score': -math.inf}
        else:
            best = {'position': None,
                    'score': math.inf}

        for possible_move in state.available_moves():
            state.make_move(possible_move, player)

            '''recursive simulate score'''
            sim_score = self.minimax(state, oponent)

            state.board[possible_move] = ' '
            state.current_winner = None
            sim_score['position'] = possible_move

            if player == max_player:
                if sim_score['score'] > best['score']:
                    best = sim_score
            else:
                if sim_score['score'] < best['score']:
                    best = sim_score

        return best
