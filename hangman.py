import random
import string

from words import words


def hangman():
    word = random.choice(words)
    word_letters = set(word)
    alphabet = set(string.ascii_uppercase)
    used_letters = set()
    lives = 7

    while len(word_letters) > 0 and lives > 0:
        if len(used_letters) > 0:
            print(f'You used thise: ', ' ,'.join(used_letters))

        guessing = [
            letter if letter in used_letters else '-' for letter in word.upper()]
        print(f'Current word, ', ' '.join(guessing))

        user_letter = input('Guess a letter: ').upper()
        if user_letter in alphabet - used_letters:
            used_letters.add(user_letter)
            if user_letter in word_letters:
                word_letters.remove(user_letter)
            else:
                lives = lives - 1
                print("Letter is not in word")
        elif (user_letter in used_letters):
            print('You already tryed this...')
        else:
            print('Please enter valid character')

    if lives == 0:
        print('Sorry, you died!')
    else:
        print('You guess the word', word, '!!')


hangman()
