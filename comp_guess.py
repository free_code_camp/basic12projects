import random


""" def computer_guess(x):
    random_number = int(input('Rundom number: '))
    computer_guess = 0
    while (computer_guess != random_number):
        computer_guess = random.randint(1, x)
        if computer_guess != random_number:
            print("I cant guess. I'm just computer...")
    print(f'Yey! I guess. It is {computer_guess}') """


def computer_guess(x):
    low = 1
    high = x
    feedback = ''
    while feedback != 'c':
        if low != high:
            computer_guess = random.randint(low, high)
        else:
            computer_guess = low
        feedback = input(
            f'Is your number {computer_guess} too high(H), or too low(L)').lower()
        if feedback == 'h':
            print("Number is too high.")
            high = computer_guess - 1
        elif feedback == 'l':
            print("Number is too low.")
            low = computer_guess + 1

    feedback = 'c'
    print(f'Yey! I guess. It is {computer_guess}!')


computer_guess(23)
